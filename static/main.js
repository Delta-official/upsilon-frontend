var ns = new function() {
    var craftDataGlobal = null
    var bodyDataGlobal = null
    var submissionValid = null
    this.boolToYesNo = function(bool) {
        if (bool) {
            return "Yes"
        }
        return "No"
    }
    this.onSubmitSavefile = function() {
        var mainContent = document.getElementById("main-element")
        var fileStatus = document.getElementById("file_status")
        var fileProgressContainer = document.getElementById("file_progress_container")
        var fileProgress = document.getElementById("file_progress")
        var fileFormData = new FormData()
        var sfsFile = document.getElementById("upload_sfs").files[0]
        if (sfsFile === undefined) {
            alert("No valid file ready to upload")
            return
        }
        if (!sfsFile.name.endsWith(".sfs")) {
            alert("Only sfs files can be uploaded")
            return
        }
        fileFormData.append("sfsfile", sfsFile)
        var xhttp = new XMLHttpRequest()
        xhttp.onreadystatechange = function() {
            console.log(this.readyState)
            if (this.readyState === 4) {
                if (this.status === 200) {
                    var response = JSON.parse(xhttp.responseText)
                    if (response["craft"] === []) {
                        mainContent.innerHTML = "No crafts found in file"
                    }
                    else {
                        mainContent.innerHTML = response["html"]
                        craftDataGlobal = response["craft"]
                        M.AutoInit()
                    }
                }
                else {
                    mainContent.innerHTML = xhttp.responseText
                    console.log("Unexpected response code")
                }
            }
        }
        xhttp.upload.onprogress = function(ev) {
            var percentLoaded = Math.round((ev.loaded / ev.total) * 100)
            if (percentLoaded === 100) {
                fileStatus.textContent = "Server processing file (this can take a long time)"
                fileProgressContainer.style = "display: none"
            }
            fileProgress.value = percentLoaded
        }
        xhttp.open("POST", "/deliver/craft", true)
        xhttp.send(fileFormData)
        fileProgressContainer.style = ""
        fileStatus.textContent = "Uploading file"
    }
    this.onNewFile = function(ev) {
        ev.preventDefault()
        var sfsFile = document.getElementById("upload_sfs").files[0]
        var fileStatus = document.getElementById("file_status")
        var selectedFile = document.getElementById("upload_file_selected")
        if (sfsFile === undefined) {
            selectedFile.textContent = "Nothing"
            fileStatus.textContent = "Idle"
            return
        }
        if (!sfsFile.name.endsWith(".sfs")) {
            selectedFile.textContent = "Invalid file - not an sfs file"
            fileStatus.textContent = "Not ready"
            return
        }
        selectedFile.textContent = sfsFile.name
        fileStatus.textContent = "Ready"
    }
    this.onNewScreenshot = function(ev) {
        ev.preventDefault()
        var screenshots = document.getElementById("upload_screenshot")
        var n_screenshots = document.getElementById("n_screenshots")
        var screenshots_valid = document.getElementById("screenshots_valid")
        var reasons = []
        var totalSize = 0
        for (let fileName of screenshots.files) {
            if (fileName.name.includes(".")) {
                if (["jpeg", "png", "jpg"].includes(fileName.name.split(".").slice(-1)[0].toLowerCase())) {
                    if (fileName.size > 8 * 1024 ** 2) {
                        reasons.push("Single file size >8MB")
                    }
                    totalSize += fileName.size
                }
                else {
                    reasons.push("Bad file extension")
                }
            }
            else {
                reasons.push("Bad file extension")
            }
        }
        if (totalSize > 50 * 1024 ** 2) {
            reasons.push("Total file size >50MB")
        }
        if (screenshots.files.length === 0) {
            reasons.push("Must upload 1 or more screenshots")
        }
        reasons = Array.from(new Set(reasons))
        if (reasons.length > 0) {
            n_screenshots.textContent = ""
            screenshots_valid.textContent = "No, " + reasons.join(", ")
            submissionValid = false
            return
        }
        n_screenshots.textContent = screenshots.files.length
        screenshots_valid.textContent = "Yes"
        submissionValid = true

    }
    this.onSubmitScreenshot = function(reupload) {
        var mainContent = document.getElementById("main-element")
        var fileProgress = document.getElementById("file_progress")
        var fileFormData = new FormData()
        var screenshots = document.getElementById("upload_screenshot").files
        if (!submissionValid) {
            alert("Screenshots invalid")
            return
        }
        for (let file of screenshots) {
            fileFormData.append('screenshots[]', file);
        }
        var currentCraft = document.getElementById("craft_selector").value
        if (currentCraft === "") {
            alert("No craft selected")
            return
        }
        var craftData = craftDataGlobal[currentCraft]
        if (craftData["invalidation_reasons"].length !== 0) {
            alert("Vessel invalid")
            return
        }
        if (craftData["situation"] === "LANDED") {
            if (!confirm(`The craft you have chosen is currently landed. Please confirm that you ` +
                          `used the "High" terrain detail setting, as otherwise your craft will ` +
                          `clip into the the ground when others try to load it`)) {
                return
            }
        }
        fileFormData.append("name", craftData["name"])
        if (reupload) {
            fileFormData.append("id", craftData["submission_id"])
        }
        var xhttp = new XMLHttpRequest()
        xhttp.onreadystatechange = function() {
            console.log(this.readyState)
            if (this.readyState === 4) {
                if (this.status === 200) {
                    fileProgress.style = "display: none"
                    document.location.href = "/submissions/"
                }
                else {
                    mainContent.innerHTML = xhttp.responseText
                    console.log("Unexpected response code")
                }
            }
        }
        xhttp.upload.onprogress = function(ev) {
            var percentLoaded = Math.round((ev.loaded / ev.total) * 100)
            fileProgress.value = percentLoaded
        }
        xhttp.open("POST", "/deliver/screenshot", true)
        xhttp.send(fileFormData)
        fileProgress.style = ""
    }
    this.craftDetails = function() {
        // console.log(craftDataGlobal)
        var currentCraft = document.getElementById("craft_selector").value
        if (currentCraft === "") {
            return
        }
        var craftData = craftDataGlobal[currentCraft]
        var vessel_name = document.getElementById("vessel_name")
        var vessel_upsilon_name = document.getElementById("vessel_upsilon_name")
        var vessel_location = document.getElementById("vessel_location")
        var vessel_valid = document.getElementById("vessel_valid")
        var vessel_kerbals = document.getElementById("vessel_kerbals")
        var vessel_votes = document.getElementById("vessel_votes")
        var vessel_images_link = document.getElementById("vessel_images_link")
        var vessel_mods_link = document.getElementById("vessel_mods_link")
        var vessel_approved = document.getElementById("vessel_approved")
        vessel_name.textContent = craftData["name"]
        vessel_upsilon_name.textContent = craftData["upsilon_name"]
        vessel_location.textContent = craftData["parent_name"] + ", " + craftData["situation"]
        if (vessel_kerbals != null) {
            vessel_kerbals.textContent = craftData["kerbals"]["crew"].join(", ")
            if (craftData["kerbals"]["crew"].length !== 0) {
                document.getElementById("vessel_kerbals_container").style = ""
            }
            else {
                document.getElementById("vessel_kerbals_container").style = "display: none"
            }
        }
        if (vessel_valid != null) {
            vessel_valid.textContent = ns.boolToYesNo(craftData["invalidation_reasons"].length === 0)
            document.getElementById("vessel_invalid_reasons").innerHTML = ""
            if (craftData["invalidation_reasons"].length !== 0) {
                for (let reason of craftData["invalidation_reasons"]) {
                    let newElement = document.createElement("div")
                    newElement.className = "invalid_reason"
                    newElement.innerHTML = reason
                    document.getElementById("vessel_invalid_reasons").appendChild(newElement)
                }
                document.getElementById("vessel_invalid_container").style = ""
            }
            else {
                document.getElementById("vessel_invalid_container").style = "display: none"
            }
        }
        if (vessel_votes != null) {
            vessel_votes.textContent = "Positive: " + craftData["positive_votes"] + " Negative: " + craftData["negative_votes"]
        }
        if (vessel_images_link != null) {
            vessel_images_link.textContent = "Screenshots"
            vessel_images_link.href = craftData["images_link"]
        }
        if (vessel_mods_link != null) {
            vessel_mods_link.textContent = "Mods"
            vessel_mods_link.href = craftData["mods_link"]
        }
        if (vessel_approved != null) {
            vessel_approved.textContent = ns.boolToYesNo(craftData["approved"])
        }
    }
    this.deleteSubmission = function() {
        var mainContent = document.getElementById("main-element")
        var currentCraft = document.getElementById("craft_selector").value
        if (currentCraft === "") {
            alert("No craft selected")
            return
        }
        var craftData = craftDataGlobal[currentCraft]
        var delete_confirm = confirm("Permanently delete submission?")
        if (!delete_confirm) {
            return
        }
        var xhttp = new XMLHttpRequest()
        xhttp.onreadystatechange = function() {
            console.log(this.readyState)
            if (this.readyState === 4) {
                if (this.status === 200) {
                    document.location.href = "/submissions/"
                }
                else {
                    mainContent.innerHTML = xhttp.responseText
                    console.log("Unexpected response code")
                }
            }
        }
        xhttp.open("POST", "/submissions/delete", true)
        xhttp.setRequestHeader("Content-Type", "application/json")
        xhttp.send(JSON.stringify({"submission_id": craftData["submission_id"]}))
    }
    this.renameCraft = function() {
        var mainContent = document.getElementById("main-element")
        var currentCraft = document.getElementById("craft_selector").value
        if (currentCraft === "") {
            alert("No craft selected")
            return
        }
        var craftData = craftDataGlobal[currentCraft]
        var currentCraft = document.getElementById("craft_selector").value
        var newName = document.getElementById("vessel_new_name")
        var xhttp = new XMLHttpRequest()
        xhttp.onreadystatechange = function() {
            console.log(this.readyState)
            if (this.readyState === 4) {
                if (this.status === 200) {
                    document.location.href = "/submissions/"
                }
                else {
                    mainContent.innerHTML = xhttp.responseText
                    console.log("Unexpected response code")
                }
            }
        }
        xhttp.open("POST", "/submissions/rename", true)
        xhttp.setRequestHeader("Content-Type", "application/json")
        xhttp.send(JSON.stringify({"submission_id": craftData["submission_id"], "name": newName.value}))
    }
    this.unhide = function(element) {
        document.getElementById(element).style = ""
    }
    this.setCraftData = function(data) {
        craftDataGlobal = data
    }
    this.setBodyData = function(data) {
        bodyDataGlobal = data
    }
    this.bodyDetails = function() {
        var currentBody = document.getElementById("body_selector").value
        var currentSizeLimit = document.getElementById("size_limit_selector").value
        var n_crafts = document.getElementById("n_crafts")
        if (currentBody === "") {
            return
        }
        var bodyData = bodyDataGlobal[currentBody]
        var sizeData
        var includeSubbodies = document.getElementById("include_subbodies").checked
        if (includeSubbodies) {
            sizeData = bodyData["n_crafts_subbodies"]
        } else {
            sizeData = bodyData["n_crafts"]
        }
        n_crafts.textContent = sizeData[currentSizeLimit]
    }


    this.downloadSaveFile = function() {
        var body = document.getElementById("body_selector").value
        var limit = document.getElementById("size_limit_selector").value
        var status = document.getElementById("download_status")
        var mainContent = document.getElementById("main-element")
        var button = document.getElementById("download_button")
        if (body === "") {
            alert("You must select a body")
            return
        }
        if (document.getElementById("n_crafts").textContent === "0") {
            alert("No crafts to download")
            return
        }
        button.disabled = true
        status.textContent = "Preparing file - please wait"
        var includeSubbodies = document.getElementById("include_subbodies").checked
        var downloadUrl = "/export?body=" + body + "&include_subbodies=" + includeSubbodies
        if (limit !== "No limit") {
            downloadUrl += "&limit=" + limit
        }

        var xhttp = new XMLHttpRequest()
        xhttp.onreadystatechange = function() {
            console.log(this.readyState)
            if (this.readyState === 3) {
                status.textContent = "Downloading"
            } else if (this.readyState === 4) {
                if (this.status === 200) {
                    let a = document.createElement("a")
                    a.download = "upsilon_export.sfs"
                    a.href = window.URL.createObjectURL(xhttp.response)
                    document.body.appendChild(a)
                    a.click()
                    document.body.removeChild(a)
                    button.disabled = false
                    status.textContent = "Idle"
                }
                else {
                    mainContent.innerHTML = xhttp.responseText
                    console.log("Unexpected response code")
                }
            }
        }
        xhttp.open("GET", downloadUrl, true)
        xhttp.responseType = "blob"
        xhttp.send()
    }
    this.discordLogin = function() {
        location.href = "/discord/login"
    }
}


