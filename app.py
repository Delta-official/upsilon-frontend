# pylint: disable=logging-format-interpolation
import copy
import logging
import multiprocessing
import os
import shutil
import time

import flask
import flask_sqlalchemy
import natsort
import magic
import sfsutils
import sqlalchemy.orm.attributes
import ujson as json
import werkzeug

import craft_handler

APP = flask.Flask(__name__)
APP.config.update(
    SESSION_COOKIE_SECURE=True,
    SESSION_COOKIE_HTTPONLY=True,
    SESSION_COOKIE_SAMESITE="Lax"
)


def create_sublogger(level, path):
    """Sets up a sublogger"""
    formatter = logging.Formatter("%(asctime)s %(name)s %(process)d %(levelname)s %(message)s")
    logger_handler = logging.FileHandler(path)
    logger_handler.setLevel(level)
    logger_handler.setFormatter(formatter)
    return logger_handler

ROOT_LOGGER = logging.getLogger()
ROOT_LOGGER.setLevel(logging.DEBUG)
os.makedirs("logs", exist_ok=True)
ROOT_LOGGER.addHandler(create_sublogger(logging.INFO, "logs/upsilon_main.log"))
LOGGER = logging.getLogger("upsilon_main")

SECRETS = {}
try:
    from flask_discord import DiscordOAuth2Session
    with open("secrets.json", "r") as secrets_file:
        SECRETS = json.load(secrets_file)
    APP.secret_key = SECRETS["app"].encode()
    APP.config["DISCORD_CLIENT_ID"] = 595265649727766548
    APP.config["DISCORD_CLIENT_SECRET"] = SECRETS["discord"]
    APP.config["DISCORD_REDIRECT_URI"] = "https://upsilon.cryptgrapher.com/discord/callback"
    DISCORD = DiscordOAuth2Session(APP)
    APP.config["SQLALCHEMY_DATABASE_URI"] = SECRETS["database_connect_string"]
except (FileNotFoundError, ImportError):
    APP.secret_key = b"qwerty"
    LOGGER.info("Using dummy discord / database configuration")
    class DiscordDummy:
        # pylint: disable=missing-docstring
        def __init__(self):
            self.authorized = True
        def fetch_user(self):
            return DiscordUser()
        def callback(self):
            pass
        def create_session(self, **kwargs):
            pass
    DISCORD = DiscordDummy()
    APP.config["SQLALCHEMY_DATABASE_URI"] = "sqlite://"


APP.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
DB = flask_sqlalchemy.SQLAlchemy(APP)

class Submission(DB.Model):
    """Submission database table model"""
    # pylint: disable=no-member
    submission_id = DB.Column(DB.Integer, autoincrement=True, primary_key=True)
    discord_id = DB.Column(DB.BigInteger, nullable=False)
    message_id = DB.Column(DB.BigInteger) # added by bot
    craft_name = DB.Column(DB.String(), nullable=False)
    craft_details_json = DB.Column(DB.JSON, nullable=False)
    warnings_json = DB.Column(DB.JSON, nullable=False)
    files_path = DB.Column(DB.String()) # added on second id pass
    positive_votes = DB.Column(DB.Integer, nullable=False)
    negative_votes = DB.Column(DB.Integer, nullable=False)
    approved = DB.Column(DB.Boolean, nullable=False)

DB.create_all()

with open("config.json", "r") as config_file:
    CONFIG = json.load(config_file)
    DATA_STORAGE_PATH = CONFIG["DATA_STORAGE_PATH"]
    SUBMISSIONS_PATH = CONFIG["SUBMISSIONS_PATH"]
    IMAGES_PATH = CONFIG["IMAGES_PATH"]
    TEMP_SAVEFILE_PATH = CONFIG["TEMP_SAVEFILE_PATH"]
    # used for screenshot links in database
    SITE_LINK = CONFIG["SITE_LINK"]
TEMPLATE_SFS = "upsilon_export.sfs"

SCREENSHOT_EXTENSIONS = {"png", "jpg", "jpeg"}
SCREENSHOT_MIMES = {"image/png", "image/jpeg"}

class ConnectionWrapper:
    # pylint: disable=missing-docstring
    """Connection wrapper"""
    def __init__(self, address=None, family=None, authkey=None):
        self.address = address
        self.family = family
        self.authkey = authkey
        self.connection = None
        if address is not None:
            self.open_connection()
    def open_connection(self):
        while True:
            try:
                self.connection = multiprocessing.connection.Client(
                    self.address, self.family, self.authkey
                )
            except Exception:
                LOGGER.error("IPC connect failed")
                time.sleep(5)
            else:
                LOGGER.info("IPC connect successful")
                return
    def send(self, data):
        if self.address is not None:
            LOGGER.debug("IPC send begin")
            while True:
                try:
                    LOGGER.debug("IPC send data")
                    self.connection.send(data)
                    return
                except ConnectionError:
                    LOGGER.error("IPC send failed")
                    self.open_connection()


if "bot_socket" in SECRETS:
    LOGGER.debug("Attempting to open ipc client")
    BOT_CONNECTION = ConnectionWrapper(
        CONFIG["socket_location"], family="AF_UNIX", authkey=SECRETS["bot_socket"].encode())
else:
    BOT_CONNECTION = ConnectionWrapper()

class DiscordUser:
    """Dummy discord user"""
    def __init__(self, name="Test", discriminator="0001", verified=True, discord_id=123456789):
        self.name = name
        self.discriminator = discriminator
        self.verified = verified
        self.id = discord_id

    def __str__(self):
        return "{0}#{1}".format(self.name, self.discriminator)


class SessionStorage:
    """Stores user local information"""
    def __init__(self, session_id):
        self.folder = os.path.join(DATA_STORAGE_PATH, TEMP_SAVEFILE_PATH, str(session_id))
        if not os.path.exists(self.folder):
            os.makedirs(self.folder)
    def remove_data(self, file, remove_folder=False):
        """Removes data from the specified file, and the whole session folder if specified"""
        if os.path.exists(os.path.join(self.folder, file)):
            os.remove(os.path.join(self.folder, file))
        if remove_folder:
            os.rmdir(self.folder)
    def write_data(self, data, file):
        """Writes data to the specified file"""
        with open(os.path.join(self.folder, file), "w") as data_file:
            json.dump(data, data_file)
    def read_data(self, file):
        """Reads data from the specified file"""
        with open(os.path.join(self.folder, file), "r") as data_file:
            # return json.load(data_file, object_pairs_hook=collections.OrderedDict)
            return json.load(data_file)


def gen_link(page, submission_id):
    """Generate link for page given submission id"""
    return "{0}/{1}/{2}/".format(SITE_LINK, page, submission_id)

def discord_info():
    """Fetches discord info and caches it in the flask session"""
    return DISCORD.fetch_user()

def discord_verified_checker(user, response_type="page", tab=0):
    """Checks if a user's discord account is verified (currently not working)

    The discord oauth scope may need to be changed for this to work, or the flask-discord
    library may be broken.
    """
    if not user.verified:
        if response_type == "page":
            return (flask.render_template("error.html", error_message=DISCORD_AUTH_ERROR, tab=tab), 403)
        return (DISCORD_AUTH_ERROR, 403)
    return False

def filter_raw(craft):
    """Filters the raw key from the craft dictionary"""
    return {k: v for k, v in craft.items() if k != "raw"}


@APP.route("/")
def home_page():
    """Home page"""
    options = dict(discord_auth=DISCORD.authorized)
    if DISCORD.authorized:
        user = discord_info()
        LOGGER.info("Homepage from {0}".format(user))
        options["discord_name"] = user.name
    return flask.render_template("homepage.html", tab=0, **options)

@APP.route("/submit/")
def submit_savefile_page():
    """Savefile submission page"""
    if not DISCORD.authorized:
        return (flask.render_template("error.html", tab=2, error_message=DISCORD_AUTH_ERROR),
                403)
    return flask.render_template("savefile_upload.html", discord_auth=True, tab=3)

@APP.route("/submissions/")
def view_submissions_page():
    """Submissions view page"""
    if not DISCORD.authorized:
        return (flask.render_template("error.html", error_message=DISCORD_AUTH_ERROR, tab=2),
                403)
    user = discord_info()
    submissions = Submission.query.filter_by(discord_id=user.id).all()
    craft_dict = {}
    for item in submissions:
        craft_dict[item.craft_name] = item.craft_details_json
        craft_dict[item.craft_name].update(
            dict(positive_votes=item.positive_votes, negative_votes=item.negative_votes,
                 submission_id=item.submission_id, approved=item.approved,
                 mods_link=gen_link("mods", item.submission_id),
                 images_link=gen_link("screenshots", item.submission_id))
        )
    return flask.render_template("submissions_view.html", discord_auth=True, craft_dict=craft_dict, tab=2)

@APP.route("/signout/")
def signout_page():
    """Discord signout page"""
    if not DISCORD.authorized:
        return (flask.render_template("error.html", error_message="Already signed out", tab=4),
                400)
    DISCORD.revoke()
    return flask.redirect(flask.url_for("home_page"))



@APP.route("/submissions/rename", methods=["POST"])
def rename_craft():
    """Endpoint for submission renames"""
    if not DISCORD.authorized:
        return (DISCORD_AUTH_ERROR, 403)
    user = discord_info()
    post_data = flask.request.json
    submission = Submission.query.filter_by(submission_id=post_data["submission_id"]).first()
    if submission is None:
        return ("Submission not found", 404)
    if submission.discord_id != user.id:
        return ("Submission does not belong to you (stop messing with things!)", 403)
    # sanitise craft name
    craft_name = werkzeug.utils.secure_filename(post_data["name"])
    if not craft_name:
        return ("Craft name cannot be blank", 400)
    if DB.session.query(DB.exists().where(Submission.craft_name == craft_name)).scalar():
        return ("Craft name {0} already taken - if the submission is yours you can delete or "
                "rename it".format(craft_name), 400)
    submission.craft_name = craft_name
    submission.craft_details_json["name"] = craft_name
    # flag needed due to dict mutability
    sqlalchemy.orm.attributes.flag_modified(submission, "craft_details_json")
    DB.session.commit()
    BOT_CONNECTION.send(dict(action="rename_submission", name=craft_name,
                             message_id=submission.message_id))
    return ""

@APP.route("/submissions/delete", methods=["POST"])
def delete_craft():
    """Endpoint for submission deletion"""
    if not DISCORD.authorized:
        return (DISCORD_AUTH_ERROR, 403)
    user = discord_info()
    post_data = flask.request.json
    submission = Submission.query.filter_by(submission_id=post_data["submission_id"]).first()
    if submission is None:
        return ("Submission not found (already deleted?)", 404)
    if submission.discord_id != user.id:
        return ("Submission does not belong to you (stop messing with things!)", 403)
    LOGGER.info("Deleting submission {0} from {1}".format(post_data["submission_id"], user))
    files_path = submission.files_path
    DB.session.delete(submission)
    DB.session.commit()
    shutil.rmtree(files_path)
    BOT_CONNECTION.send(dict(action="delete_submission", message_id=submission.message_id))
    return ""


@APP.route("/deliver/craft", methods=["POST"])
def handle_submission():
    """Endpoint for save file handling"""
    if not DISCORD.authorized:
        return (DISCORD_AUTH_ERROR, 403)
    user = discord_info()
    # unverified = discord_verified_checker(response_type="body")
    # if unverified:
    #     return unverified
    session = SessionStorage(user.id)
    data = flask.request.files["sfsfile"].read().decode()
    data = data.replace("\r", "").replace("\n ", "\n").strip()
    try:
        save = sfsutils.parse_savefile(data, sfs_is_path=False)
    except Exception:
        LOGGER.exception("SFS parse failed")
        LOGGER.error("Error parsing safefile from {0}".format(user))
        return ("Error parsing savefile - the savefile format is invalid. "
                "Check that the file is not corrupted or modified incorrectly", 400)
    try:
        processed_crafts = craft_handler.process_savefile(save)
    except Exception:
        LOGGER.exception("Craft parse failed")
        LOGGER.warning("Error parsing crafts from {0}".format(user))
        return ("Error checking savefile: Please request to have a staff member check your save. This is an internal server error that isn't your fault", 500)
    LOGGER.warning("Warnings: {0}".format(processed_crafts["warnings"]))
    if processed_crafts["exit_invalid"]:
        return json.dumps(dict(
            html=flask.render_template("craft_submit.html", invalid=processed_crafts["warnings"], tab=3),
            crafts=[]))
    session.write_data(processed_crafts, "temp.json")
    for name, craft in processed_crafts["crafts"].items():
        if craft["invalidation_reasons"]:
            LOGGER.warning("Craft {0} invalid: {1}".format(name, craft["invalidation_reasons"]))
    options = dict(craft_dict=processed_crafts["crafts"], invalid=False)
    return json.dumps(dict(html=flask.render_template("craft_submit.html", **options),
                           craft={k: filter_raw(v) for k, v in processed_crafts["crafts"].items()}))


@APP.route("/deliver/screenshot", methods=["POST"])
def handle_screenshots():
    """Endpoint for screenshot handling"""
    if not DISCORD.authorized:
        return (DISCORD_AUTH_ERROR, 403)
    user = discord_info()
    session = SessionStorage(user.id)
    data = flask.request.files.getlist("screenshots[]")
    if not data:
        return ("Error: Must upload 1 or more screenshots", 400)
    craft_name = flask.request.form["name"]
    secure_filenames = []
    total_size = 0
    for index, file in enumerate(data):
        filename = werkzeug.utils.secure_filename(file.filename)
        if filename.count(".") != 1:
            return ("Error: File {0} has an invalid name".format(filename), 400)
        name, extension = filename.split(".")
        extension = extension.lower()
        if extension not in SCREENSHOT_EXTENSIONS:
            return ("Error: File {0} has an invalid name".format(filename), 400)
        filename = ".".join((name, extension))
        secure_filenames.append(filename)
        if magic.from_buffer(file.read(1024), mime=True) not in SCREENSHOT_MIMES:
            return ("Error: File {0} appears to be of invalid type".format(filename), 400)
        file.stream.seek(0, os.SEEK_END)
        file_length = file.stream.tell()
        if file_length > 8 * 1024 ** 2:
            return ("Error: File {0} is >8MB".format(filename), 400)
        total_size += file_length
        file.stream.seek(0)
    if total_size > 50 * 1024 ** 2:
        return ("Error: Total file size is >50MB", 400)
    if "id" in flask.request.form:
        submission = Submission.query.filter_by(submission_id=flask.request.form["id"]).first()
        if submission is None:
            return ("Submission not found", 404)
        if submission.discord_id != user.id:
            return ("Submission does not belong to you (stop messing with things!)", 403)
        images_path = os.path.join(submission.files_path, IMAGES_PATH)
        for file in os.listdir(images_path):
            os.remove(os.path.join(images_path, file))
    else:
        # sanitise craft name
        new_craft_name = werkzeug.utils.secure_filename(craft_name)
        if not new_craft_name:
            return ("Craft name cannot be blank", 400)
        if DB.session.query(DB.exists().where(Submission.craft_name == new_craft_name)).scalar():
            return ("Craft name {0} already taken - if the submission is yours you can delete it "
                    " or reupload screenshots on the submissions page".format(new_craft_name), 400)
        processed_crafts = session.read_data("temp.json")
        processed_crafts["crafts"][craft_name]["name"] = new_craft_name
        session.remove_data("temp.json", remove_folder=True)
        if processed_crafts["crafts"][craft_name]["invalidation_reasons"]:
            return ("Error: Invalid craft submitted", 400)
        submission = Submission(
            discord_id=user.id, craft_name=new_craft_name, positive_votes=0, negative_votes=0,
            craft_details_json=filter_raw(processed_crafts["crafts"][craft_name]),
            warnings_json=processed_crafts["warnings"], approved=False)
        DB.session.add(submission)
        # flushing allows the submission id to be assigned without committing it to the database
        DB.session.flush()
        submission.files_path = os.path.abspath(os.path.join(DATA_STORAGE_PATH, SUBMISSIONS_PATH,
                                                             str(submission.submission_id)))
        images_path = os.path.join(submission.files_path, IMAGES_PATH)
        os.makedirs(images_path)
        kerbals = filter_roster(processed_crafts["ROSTER"], submission.craft_details_json["kerbals"]["crew"])
        processed_crafts["crafts"][craft_name]["raw"]["KERBAL"] = kerbals
        with open(os.path.join(submission.files_path, "craft.json"), "w") as craft_file:
            json.dump(processed_crafts["crafts"][craft_name]["raw"], craft_file)
    for index, file in enumerate(data):
        filename = secure_filenames[index]
        file.save(os.path.join(images_path, filename))
    DB.session.commit()
    if "id" not in flask.request.form: # don't post screenshot reuploads
        BOT_CONNECTION.send(dict(action="post_submission", submission_id=submission.submission_id))
    return ""

def filter_roster(roster, kerbals):
    out = []
    for savefile_kerbal in roster["KERBAL"]:
        if savefile_kerbal["name"] in kerbals:
            out.append(savefile_kerbal)
    return out


@APP.route("/download/")
def download_page():
    """Craft file downloads page"""
    categorisations = list(craft_handler.SIZES.values())[1:]
    bodies = copy.deepcopy(craft_handler.SOLAR_SYSTEM)
    for body in bodies.values():
        body["n_crafts"] = {x: 0 for x in categorisations}
        body["n_crafts_subbodies"] = {x: 0 for x in categorisations}
    crafts = Submission.query.filter_by(approved=True).all()
    for craft in crafts:
        if "exclude" in craft.craft_details_json:
            continue
        inspect_parent = craft.craft_details_json["parent_name"]
        size = craft.craft_details_json["upsilon_name"][0]
        bodies[inspect_parent]["n_crafts"][size] += 1
        while inspect_parent is not None:
            bodies[inspect_parent]["n_crafts_subbodies"][size] += 1
            inspect_parent = bodies[inspect_parent]["parent"]
    for body, body_data in bodies.items():
        for classifier in ("n_crafts", "n_crafts_subbodies"):
            body_n_dict = body_data[classifier]
            new_sizes = {}
            for size in body_n_dict:
                size_index = categorisations.index(size)
                new_sizes[size] = sum(v for k, v in body_n_dict.items() if size_index >= categorisations.index(k))
            new_sizes["No limit"] = sum(body_n_dict.values())
            bodies[body][classifier] = new_sizes
    size_limits = {x: False for x in categorisations[:-1]}
    size_limits["No limit"] = True
    return flask.render_template("downloads.html", body_dict=bodies, size_limits=size_limits, tab=1,
                                 discord_auth=DISCORD.authorized)

@APP.route("/export")
def export_sfs():
    """Exports crafts as an sfs file for download"""
    # LOGGER.info("Export begin")
    categorisations = list(craft_handler.SIZES.values())[1:]
    args = flask.request.args.to_dict()
    bodies = craft_handler.SOLAR_SYSTEM
    selected_body = args["body"]
    limit = args.get("limit", None)
    categorisations = list(craft_handler.SIZES.values())[1:]
    if limit is not None:
        limit = categorisations.index(limit)
    check_bodies = []
    if args["include_subbodies"] == "true":
        for body in bodies:
            parent = body
            while parent is not None:
                if parent == selected_body:
                    check_bodies.append(body)
                parent = bodies[parent]["parent"]
    else:
        check_bodies.append(selected_body)
    crafts = Submission.query.filter_by(approved=True).all()
    # LOGGER.info("Queried crafts")
    selected_crafts = []
    for craft in crafts:
        if craft.craft_details_json["parent_name"] in check_bodies:
            if limit is not None:
                if categorisations.index(craft.craft_details_json["upsilon_name"][0]) > limit:
                    continue
            selected_crafts.append(craft)
    craft_files = []
    template = sfsutils.parse_savefile(TEMPLATE_SFS)
    dummy_kerbal_template = template["GAME"]["ROSTER"]["KERBAL"][0]
    kerbals = []
    for craft in selected_crafts:
        with open(os.path.join(craft.files_path, "craft.json"), "r") as craft_file_handle:
            # data = json.load(craft_file_handle, object_pairs_hook=collections.OrderedDict)
            data = json.load(craft_file_handle)
            data["name"] = "{0} - {1}".format(craft.craft_details_json["upsilon_name"], craft.craft_name)
            if "KERBAL" in data:
                kerbals.extend(data.pop("KERBAL"))
            else:
                for kerbal_name in craft.craft_details_json["kerbals"]["crew"]:
                    dummy_kerbal = copy.deepcopy(dummy_kerbal_template)
                    dummy_kerbal["name"] = kerbal_name
                    kerbals.append(dummy_kerbal)
            craft_files.append(data)
    # LOGGER.info("Loaded data")
    template["GAME"]["FLIGHTSTATE"]["VESSEL"] = craft_files
    template["GAME"]["FLIGHTSTATE"]["UT"] = str(
        max(float(vessel["lastUT"]) for vessel in template["GAME"]["FLIGHTSTATE"]["VESSEL"])
    )
    template["GAME"]["ROSTER"]["KERBAL"].extend(kerbals)
    return sfsutils.writeout_savefile(template)

@APP.route("/mods/<int:submission_id>/")
def present_mods(submission_id):
    """Mods viewing page"""
    submission = Submission.query.filter_by(submission_id=submission_id).first()
    if submission is None:
        return (flask.render_template("error.html", error_message="Submission not found", tab=2), 404)
    return flask.render_template("mods.html", warnings=submission.warnings_json, tab=2,
                                 discord_auth=DISCORD.authorized)

@APP.route("/screenshots/<int:submission_id>/")
def present_screenshots(submission_id):
    """Screenshots viewing page"""
    images_path = os.path.join(DATA_STORAGE_PATH, SUBMISSIONS_PATH,
                               str(submission_id), IMAGES_PATH)
    if not os.path.exists(images_path):
        return (flask.render_template("error.html", error_message="Submission not found", tab=2), 404)
    files = natsort.natsorted(os.listdir(images_path))
    return flask.render_template("screenshot_view.html", files=files, tab=2,
                                 submission_id=submission_id, discord_auth=DISCORD.authorized)

@APP.route("/discord/login/")
def login():
    """Discord session creation redirect"""
    return DISCORD.create_session(scope=["identify"])

@APP.route("/discord/callback/")
def callback():
    """Discord session creation callback"""
    DISCORD.callback()
    return flask.redirect(flask.url_for("home_page"))

DISCORD_AUTH_ERROR = "Must be discord authorised"

LOGGER.info("Server booted")
